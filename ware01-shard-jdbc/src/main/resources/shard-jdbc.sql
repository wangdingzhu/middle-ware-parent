CREATE TABLE table_one (
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '主键ID',
	phone VARCHAR(20) NOT NULL COMMENT '手机号',
	back_one VARCHAR(50) DEFAULT NULL COMMENT '备用1',
	back_two VARCHAR(50) DEFAULT NULL COMMENT '备用2',
	back_three VARCHAR(50) DEFAULT NULL COMMENT '备用3',
	KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表1';

CREATE TABLE table_two (
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '主键ID',
	phone VARCHAR(20) NOT NULL COMMENT '手机号',
	back_one VARCHAR(50) DEFAULT NULL COMMENT '备用1',
	back_two VARCHAR(50) DEFAULT NULL COMMENT '备用2',
	back_three VARCHAR(50) DEFAULT NULL COMMENT '备用3',
	KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表2';

CREATE TABLE table_three (
	id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '主键ID',
	phone VARCHAR(20) NOT NULL COMMENT '手机号',
	back_one VARCHAR(50) DEFAULT NULL COMMENT '备用1',
	back_two VARCHAR(50) DEFAULT NULL COMMENT '备用2',
	back_three VARCHAR(50) DEFAULT NULL COMMENT '备用3',
	KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表3';


-- 分表：可以数据库编程建表，或者程序建表
CREATE TABLE `table_one_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `table_one_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_one_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_one_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_one_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_two_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `table_two_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_two_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_two_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `table_two_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `back_one` varchar(50) DEFAULT NULL COMMENT '备用1',
  `back_two` varchar(50) DEFAULT NULL COMMENT '备用2',
  `back_three` varchar(50) DEFAULT NULL COMMENT '备用3',
  PRIMARY KEY (`id`),
  KEY `phoneIndex` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;


-- mine test  fill_yyyy_m
CREATE TABLE `fill_2020_1`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cjqbm` int(20) NULL DEFAULT NULL,
  `cszl` double NULL DEFAULT NULL,
  `czjz` double NULL DEFAULT NULL,
  `czkssj` datetime(0) NOT NULL,
  `czyxsj` int(11) NULL DEFAULT NULL,
  `czzt` int(11) NULL DEFAULT NULL,
  `del_flag` int(11) NULL DEFAULT 0,
  `ggxh` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gpbm` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kfbm` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mbzl` double NULL DEFAULT NULL,
  `officecode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `recid` bigint(20) NULL DEFAULT NULL,
  `resouce` int(11) NULL DEFAULT NULL,
  `rfidcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tenantcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `zdbm` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `cz_date` date NOT NULL,
  `TypeName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '钢瓶型号名称',
  `CheckItem` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BottleBusinessName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '钢瓶生产厂家',
  `unit_number` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '单位内编号',
  `scrq` datetime(0) NULL DEFAULT NULL COMMENT '钢瓶生产日期',
  `scjysj` datetime(0) NULL DEFAULT NULL COMMENT '上次检验时间',
  `xcjysj` datetime(0) NULL DEFAULT NULL COMMENT '下次检验时间',
  `WaterCapacity` double(5, 2) NULL DEFAULT 0.00 COMMENT '钢瓶容积',
  `ServicePressure` double(5, 2) NULL DEFAULT 0.00 COMMENT '公称工作压力(Mpa)',
  `WorkerNumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '充装工编号',
  `WorkerName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '充装工姓名',
  `BeforeCheckStatus` int(1) NULL DEFAULT NULL COMMENT '充装前后检查状态 0合格，1不合格',
  `AfterCheckStatus` int(1) NULL DEFAULT NULL COMMENT '充装前后检查状态 0合格，1不合格',
  `type_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BeforeCheckTime` datetime(0) NULL DEFAULT NULL,
  `AfterCheckTime` datetime(0) NULL DEFAULT NULL,
  `BeforeCheckItem` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AfterCheckItem` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `temperature` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作温度',
  `checkHistoryCode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uhfChip` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超高频芯片',
  PRIMARY KEY (`id`, `czkssj`) USING BTREE,
  INDEX `index_czkssj`(`czkssj`) USING BTREE,
  INDEX `index_rfidcode`(`rfidcode`) USING BTREE,
  INDEX `index_gpbm`(`gpbm`) USING BTREE,
  INDEX `index_officecode_czkssj`(`czkssj`, `officecode`) USING BTREE,
  INDEX `index_czkssj_xpbm`(`czkssj`, `rfidcode`) USING BTREE,
  INDEX `index_cz_date`(`cz_date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;