package com.shard.jdbc.mapper;

import com.shard.jdbc.entity.FillRecordsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FillMapper {


    int insertFill(@Param(value = "info")FillRecordsVo fillRecordsVo,@Param(value = "year") int year);

    List<FillRecordsVo> queryFill(@Param(value = "info")FillRecordsVo fillRecordsVo,@Param(value = "year") int year);

    Long countFill(@Param(value = "info")FillRecordsVo fillRecordsVo,@Param(value = "year") int year);
}