package com.shard.jdbc.config;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import io.shardingsphere.api.algorithm.sharding.PreciseShardingValue;
import io.shardingsphere.api.algorithm.sharding.standard.PreciseShardingAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.Date;

/**
 * 充装分库逻辑
 */
public class DataSourceFillAlg implements PreciseShardingAlgorithm<String> {

    @Override
    public String doSharding(Collection<String> names, PreciseShardingValue<String> value) {
        Date czkssj = DateUtil.parse(String.valueOf(value.getValue()), DatePattern.NORM_DATETIME_MS_PATTERN);
        int year = DateUtil.year(czkssj);
        if(2019 == year){
            return "ds_1"; //db:shard_2019
        }
        return "ds_0";
    }
}
