package com.shard.jdbc.config;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.shard.jdbc.utils.HashUtil;
import io.shardingsphere.api.algorithm.sharding.PreciseShardingValue;
import io.shardingsphere.api.algorithm.sharding.standard.PreciseShardingAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Date;

/**
 * 充装分表算法
 */
public class TableFillAlg implements PreciseShardingAlgorithm<String> {

    private static Logger LOG = LoggerFactory.getLogger(TableFillAlg.class);


    @Override
    public String doSharding(Collection<String> names, PreciseShardingValue<String> value) {
        //fill_yyyy_m
        Date czkssj = DateUtil.parse(String.valueOf(value.getValue()),DatePattern.NORM_DATETIME_MS_PATTERN);
        int month = DateUtil.month(czkssj)+1;
        int year = DateUtil.year(czkssj);
        return "fill_"+year+"_"+ month;
    }
}
