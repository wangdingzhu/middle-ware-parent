package com.shard.jdbc.entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 充装记录VO
 * 
 * @author WEICONG
 *
 */
public class FillRecordsVo implements java.io.Serializable {

	private static final long serialVersionUID = 1187042721451228817L;
	
	/**充装记录集合**/
	private List<FillRecordsVo> fillRecordList = new ArrayList<FillRecordsVo>();

	private Long id;
	/** 所属租户编码(企业编码) **/
	private String tenantcode;
	/** 所属机构编码 **/
	private String officecode;
	/** RPID芯片编码 **/
	private String rfidcode;
	/** 一维码 **/
	private String barcode;
	/** 采集器序号 **/
	private Long recid;
	/** 采集器编码 **/
	private Integer cjqbm;
	/** 钢瓶编码 **/
	private String gpbm;
	/** 站点编码 **/
	private String zdbm;
	/** 客户编码 **/
	private String kfbm;
	/** 充装开始时间 **/
	private Date czkssj;
	/** 充装延续时间 **/
	private Integer czyxsj;
	/** 初始重量 **/
	private Double cszl;
	/** 充装净重 **/
	private Double czjz;
	/** 目标重量 **/
	private Double mbzl;
	/** 充装状态0 有效数据 ,1：充装失败数据,2：其他 **/
	private Integer czzt;
	/** 充装数据来源充装记录来源,1:MQ,2:常龙电子称 **/
	private Integer resouce;
	/** 钢瓶型号编号(规格型号) **/
	private String ggxh;
	/** 钢瓶型号名称(规格型号) */
	private String typeName;
	/** 充装前后检查项目 */
	private String checkItem;
	/** 充装前后检查状态 0合格，1不合格 */
	private Integer beforeCheckStatus;
	/** 充装前后检查状态 0合格，1不合格*/
	private Integer afterCheckStatus;
	/**
	 * 充装前检查项目
	 */
	private String beforeCheckItem;

	/**
	 * 充装后检查项目
	 */
	private String afterCheckItem;

	/**
	 * 充装前检查日期
	 */
	private Date beforeCheckTime;

	/**
	 * 充装后检查日期
	 */
	private Date afterCheckTime;
	/** 是否删除 0 :启用 1：已经删除 **/
	private Integer delFlag;

	/** 钢瓶厂家名称 */
	private String bottleBusinessName;
	/** 充装介质名称 */
	private String fillingMesonName;
	/** 生产日期 */
	private Date scrq;
	/** 上次检验时间 */
	private Date scjysj;
	/** 下次检验时间 */
	private Date xcjysj;
	/** 钢瓶容积(L) */
	private Double waterCapacity;
	/** 公称工作压力 */
	private Double servicePressure;
	/** 单位内编号 */
	private String unitNumber;
	/** 充装工编号(员工) */
	private String workerNumber;
	/** 入库时间 */
	private Date createTime;
	/** 充装工姓名(员工) */
	private String workerName;
	/***
	 * 工作温度
	 */
	private String temperature;

	/***
	 * 充装检查项版本号
	 */
	private String checkHistoryCode;

	public static void main(String[] args) {
		FillRecordsVo fillRecordsVo = new FillRecordsVo();
		Field[] fields = fillRecordsVo.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field.getName().equals("serialVersionUID")) {
				continue;
			}
			String str = field.getName();
			str = str.replaceFirst(str.substring(0, 1), str.substring(0, 1).toUpperCase());
			System.out.println("vo.set" + str + "(in.get" + str + "());");
		}

	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getCheckHistoryCode() {
		return checkHistoryCode;
	}

	public void setCheckHistoryCode(String checkHistoryCode) {
		this.checkHistoryCode = checkHistoryCode;
	}

	/**
	 * 获取 bottleBusinessName
	 */
	public String getBottleBusinessName() {
		return bottleBusinessName;
	}

	/**
	 * 设置 bottleBusinessName
	 */
	public void setBottleBusinessName(String bottleBusinessName) {
		this.bottleBusinessName = bottleBusinessName;
	}

	public String getFillingMesonName() {
		return fillingMesonName;
	}

	public void setFillingMesonName(String fillingMesonName) {
		this.fillingMesonName = fillingMesonName;
	}

	public Date getScrq() {
		return scrq;
	}

	public void setScrq(Date scrq) {
		this.scrq = scrq;
	}

	public Date getScjysj() {
		return scjysj;
	}

	public void setScjysj(Date scjysj) {
		this.scjysj = scjysj;
	}

	public Date getXcjysj() {
		return xcjysj;
	}

	public void setXcjysj(Date xcjysj) {
		this.xcjysj = xcjysj;
	}

	public Double getWaterCapacity() {
		return waterCapacity;
	}

	public void setWaterCapacity(Double waterCapacity) {
		this.waterCapacity = waterCapacity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 所属租户编码(企业编码)
	 * 
	 * @return
	 */
	public String getTenantcode() {
		return tenantcode;
	}

	/**
	 * 所属租户编码(企业编码)
	 * 
	 * @param tenantcode
	 */
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}

	/**
	 * 所属机构编码
	 * 
	 * @return
	 */
	public String getOfficecode() {
		return officecode;
	}

	/**
	 * 所属机构编码
	 * 
	 * @param officecode
	 */
	public void setOfficecode(String officecode) {
		this.officecode = officecode;
	}

	/**
	 * RPID芯片编码
	 * 
	 * @return
	 */
	public String getRfidcode() {
		return rfidcode;
	}

	/**
	 * RPID芯片编码
	 * 
	 * @param rfidcode
	 */
	public void setRfidcode(String rfidcode) {
		this.rfidcode = rfidcode;
	}

	/**
	 * 一维码
	 * 
	 * @return
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * 一维码
	 * 
	 * @param barcode
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * 采集器序号
	 * 
	 * @return recid recid
	 */
	public Long getRecid() {
		return recid;
	}

	/**
	 * 采集器序号
	 * 
	 * @param recid
	 *            recid
	 */
	public void setRecid(Long recid) {
		this.recid = recid;
	}

	/**
	 * 采集器编码
	 * 
	 * @return cjqbm cjqbm
	 */
	public Integer getCjqbm() {
		return cjqbm;
	}

	/**
	 * 采集器编码
	 * 
	 * @param cjqbm
	 *            cjqbm
	 */
	public void setCjqbm(Integer cjqbm) {
		this.cjqbm = cjqbm;
	}

	/**
	 * 钢瓶编码
	 * 
	 * @return gpbm gpbm
	 */
	public String getGpbm() {
		return gpbm;
	}

	/**
	 * 钢瓶编码
	 * 
	 * @param gpbm
	 *            gpbm
	 */
	public void setGpbm(String gpbm) {
		this.gpbm = gpbm;
	}

	/**
	 * 站点编码
	 * 
	 * @return zdbm zdbm
	 */
	public String getZdbm() {
		return zdbm;
	}

	/**
	 * 站点编码
	 * 
	 * @param zdbm
	 *            zdbm
	 */
	public void setZdbm(String zdbm) {
		this.zdbm = zdbm;
	}

	/**
	 * 客户编码
	 * 
	 * @return kfbm kfbm
	 */
	public String getKfbm() {
		return kfbm;
	}

	/**
	 * 客户编码
	 * 
	 * @param kfbm
	 *            kfbm
	 */
	public void setKfbm(String kfbm) {
		this.kfbm = kfbm;
	}

	/**
	 * 充装开始时间
	 * 
	 * @return czkssj czkssj
	 */
	public Date getCzkssj() {
		return czkssj;
	}

	/**
	 * 充装开始时间
	 * 
	 * @param czkssj
	 *            czkssj
	 */
	public void setCzkssj(Date czkssj) {
		this.czkssj = czkssj;
	}

	/**
	 * 充装延续时间
	 * 
	 * @return czyxsj czyxsj
	 */
	public Integer getCzyxsj() {
		return czyxsj;
	}

	/**
	 * 充装延续时间
	 * 
	 * @param czyxsj
	 *            czyxsj
	 */
	public void setCzyxsj(Integer czyxsj) {
		this.czyxsj = czyxsj;
	}

	/**
	 * 初始重量
	 * 
	 * @return cszl cszl
	 */
	public Double getCszl() {
		return cszl;
	}

	/**
	 * 初始重量
	 * 
	 * @param cszl
	 *            cszl
	 */
	public void setCszl(Double cszl) {
		this.cszl = cszl;
	}

	/**
	 * 充装净重
	 * 
	 * @return czjz czjz
	 */
	public Double getCzjz() {
		return czjz;
	}

	/**
	 * 充装净重
	 * 
	 * @param czjz
	 *            czjz
	 */
	public void setCzjz(Double czjz) {
		this.czjz = czjz;
	}

	/**
	 * 目标重量
	 * 
	 * @return mbzl mbzl
	 */
	public Double getMbzl() {
		return mbzl;
	}

	/**
	 * 目标重量
	 * 
	 * @param mbzl
	 *            mbzl
	 */
	public void setMbzl(Double mbzl) {
		this.mbzl = mbzl;
	}

	/**
	 * 充装状态0 有效数据 ,1：充装失败数据,2：其他
	 * 
	 * @return czzt czzt
	 */
	public Integer getCzzt() {
		return czzt;
	}

	/**
	 * 充装状态0 有效数据 ,1：充装失败数据,2：其他
	 * 
	 * @param czzt
	 *            czzt
	 */
	public void setCzzt(Integer czzt) {
		this.czzt = czzt;
	}

	/**
	 * 充装数据来源充装记录来源,1:MQ,2:常龙电子称
	 * 
	 * @return resouce resouce
	 */
	public Integer getResouce() {
		return resouce;
	}

	/**
	 * 充装数据来源充装记录来源,1:MQ,2:常龙电子称
	 * 
	 * @param resouce
	 *            resouce
	 */
	public void setResouce(Integer resouce) {
		this.resouce = resouce;
	}

	/**
	 * 钢瓶规格型号编号
	 * 
	 * @return ggxh ggxh
	 */
	public String getGgxh() {
		return ggxh;
	}

	/**
	 * 钢瓶规格型号编号
	 * 
	 * @param ggxh
	 *            ggxh
	 */
	public void setGgxh(String ggxh) {
		this.ggxh = ggxh;
	}

	/**
	 * 钢瓶型号名称(规格型号)
	 * 
	 * @return typeName typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * 钢瓶型号名称(规格型号)
	 * 
	 * @param typeName
	 *            typeName
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/** 
	 * 
	 * @return checkItem checkItem 
	 */
	public String getCheckItem() {
		return checkItem;
	}

	/** 
	 * 
	 * @param checkItem checkItem 
	 */
	public void setCheckItem(String checkItem) {
		this.checkItem = checkItem;
	}

	/**
	 * 是否删除 0 :启用 1：已经删除
	 * 
	 * @return delFlag delFlag
	 */
	public Integer getDelFlag() {
		return delFlag;
	}

	/**
	 * 是否删除 0 :启用 1：已经删除
	 * 
	 * @param delFlag
	 *            delFlag
	 */
	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	/**  
	 * 获取servicePressure  
	 */
	public Double getServicePressure() {
		return servicePressure;
	}

	/**
	 * 设置 servicePressure
	 */
	public void setServicePressure(Double servicePressure) {
		this.servicePressure = servicePressure;
	}

	/**  
	 * 获取unitNumber  
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * 设置 unitNumber
	 */
	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	/**
	 * 充装工编号(员工)
	 * @return
	 */
	public String getWorkerNumber() {
		return workerNumber;
	}

	/**
	 * 充装工编号(员工)
	 * @param workerNumber
	 */
	public void setWorkerNumber(String workerNumber) {
		this.workerNumber = workerNumber;
	}
	/**
	 * 充装记录集合
	 * 
	 */
	public List<FillRecordsVo> getFillingList() {
		return fillRecordList;
	}
	
	/**
	 * 入库时间
	 * 
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 入库时间
	 * 
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	/**
	 * 充装工姓名(员工)
	 * @return
	 */
	public String getWorkerName() {
		return workerName;
	}

	/**
	 * 充装工姓名(员工)
	 * @param workerName
	 */
	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	/**
	 * Gets the value of beforeCheckStatus
	 *
	 * @return the value of beforeCheckStatus
	 */
	public Integer getBeforeCheckStatus() {
		return beforeCheckStatus;
	}

	/**
	 * Sets the beforeCheckStatus
	 * <p>You can use getBeforeCheckStatus() to get the value of beforeCheckStatus</p>
	 *
	 * @param beforeCheckStatus beforeCheckStatus
	 */
	public void setBeforeCheckStatus(Integer beforeCheckStatus) {
		this.beforeCheckStatus = beforeCheckStatus;
	}

	/**
	 * Gets the value of afterCheckStatus
	 *
	 * @return the value of afterCheckStatus
	 */
	public Integer getAfterCheckStatus() {
		return afterCheckStatus;
	}

	/**
	 * Sets the afterCheckStatus
	 * <p>You can use getAfterCheckStatus() to get the value of afterCheckStatus</p>
	 *
	 * @param afterCheckStatus afterCheckStatus
	 */
	public void setAfterCheckStatus(Integer afterCheckStatus) {
		this.afterCheckStatus = afterCheckStatus;
	}

	/**
	 * Gets the value of beforeCheckItem
	 *
	 * @return the value of beforeCheckItem
	 */
	public String getBeforeCheckItem() {
		return beforeCheckItem;
	}

	/**
	 * Sets the beforeCheckItem
	 * <p>You can use getBeforeCheckItem() to get the value of beforeCheckItem</p>
	 *
	 * @param beforeCheckItem beforeCheckItem
	 */
	public void setBeforeCheckItem(String beforeCheckItem) {
		this.beforeCheckItem = beforeCheckItem;
	}

	/**
	 * Gets the value of afterCheckItem
	 *
	 * @return the value of afterCheckItem
	 */
	public String getAfterCheckItem() {
		return afterCheckItem;
	}

	/**
	 * Sets the afterCheckItem
	 * <p>You can use getAfterCheckItem() to get the value of afterCheckItem</p>
	 *
	 * @param afterCheckItem afterCheckItem
	 */
	public void setAfterCheckItem(String afterCheckItem) {
		this.afterCheckItem = afterCheckItem;
	}

	/**
	 * Gets the value of beforeCheckTime
	 *
	 * @return the value of beforeCheckTime
	 */
	public Date getBeforeCheckTime() {
		return beforeCheckTime;
	}

	/**
	 * Sets the beforeCheckTime
	 * <p>You can use getBeforeCheckTime() to get the value of beforeCheckTime</p>
	 *
	 * @param beforeCheckTime beforeCheckTime
	 */
	public void setBeforeCheckTime(Date beforeCheckTime) {
		this.beforeCheckTime = beforeCheckTime;
	}

	/**
	 * Gets the value of afterCheckTime
	 *
	 * @return the value of afterCheckTime
	 */
	public Date getAfterCheckTime() {
		return afterCheckTime;
	}

	/**
	 * Sets the afterCheckTime
	 * <p>You can use getAfterCheckTime() to get the value of afterCheckTime</p>
	 *
	 * @param afterCheckTime afterCheckTime
	 */
	public void setAfterCheckTime(Date afterCheckTime) {
		this.afterCheckTime = afterCheckTime;
	}
}