package com.shard.jdbc.service;

import com.hyq.gas.model.input.FillRecordsIn;
import com.shard.jdbc.entity.FillRecordsVo;
import com.shard.jdbc.entity.TableOne;
import com.shard.jdbc.entity.TableTwo;

import java.util.List;

public interface ShardService {
    void createTable () ;
    void insertOne () ;
    void insertTwo () ;
    TableOne selectOneByPhone (String phone) ;
    TableTwo selectTwoByPhone (String phone) ;

    void insertFill(FillRecordsVo fillRecordsVo);

    List<FillRecordsVo> queryFill(FillRecordsVo fillRecordsVo);

    Long countFill(FillRecordsVo fillRecordsVo);
}
