package com.shard.jdbc.controller;

import com.hyq.gas.model.input.FillRecordsIn;
import com.shard.jdbc.entity.FillRecordsVo;
import com.shard.jdbc.entity.TableOne;
import com.shard.jdbc.entity.TableTwo;
import com.shard.jdbc.service.ShardService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class ShardController {

    @Resource
    private ShardService shardService ;
    /**
     * 1、建表流程
     */
    @RequestMapping("/createTable")
    public String createTable (){
        shardService.createTable();
        return "success" ;
    }
    /**
     * 2、生成表 table_one 数据
     */
    @RequestMapping("/insertOne")
    public String insertOne (){
        shardService.insertOne();
        return "SUCCESS" ;
    }
    /**
     * 3、生成表 table_two 数据
     */
    @RequestMapping("/insertTwo")
    public String insertTwo (){
        shardService.insertTwo();
        return "SUCCESS" ;
    }
    /**
     * 4、查询表 table_one 数据
     */
    @RequestMapping("/selectOneByPhone/{phone}")
    public TableOne selectOneByPhone (@PathVariable("phone") String phone){
        return shardService.selectOneByPhone(phone);
    }
    /**
     * 5、查询表 table_one 数据
     */
    @RequestMapping("/selectTwoByPhone/{phone}")
    public TableTwo selectTwoByPhone (@PathVariable("phone") String phone){
        return shardService.selectTwoByPhone(phone);
    }


    /***
     * @param fillRecordsVo
     * eg:
     * {
     *     "rfidcode":"X1234567",
     *     "gpbm":"G88888",
     *     "tenantcode":"88011",
     *     "zdbm":"QZ01",
     *     "czkssj":"2019-01-22 20:40:30"
     * }
     * @return
     */
    @PostMapping("/insertFill")
    public String insertFill (@RequestBody FillRecordsVo fillRecordsVo){
        shardService.insertFill(fillRecordsVo);
        return "SUCCESS" ;
    }


    @GetMapping("/queryFill")
    public List<FillRecordsVo> queryFill (@RequestBody FillRecordsVo fillRecordsVo){
        System.out.println("count:"+shardService.countFill(fillRecordsVo));
        return shardService.queryFill(fillRecordsVo) ;
    }

}
